import { Component, OnInit, OnDestroy } from '@angular/core';

import { AppareilService } from '../services/appareil.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-appareils-list-view',
  templateUrl: './appareils-list-view.component.html'
})
export class AppareilsListViewComponent implements OnInit, OnDestroy {

  isAuth = false;

  // Appareil en liste (array).
  appareilList: any[];
  id: number;

  // Avec Subject.
  appareilSubscription: Subscription;

  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });

  constructor(private appareilService: AppareilService) {
    // Simule une autorisation au bout de 4 secondes.
    setTimeout(
      () => {
        this.isAuth = true;
      }, 4000
    );
  }

  // Implémente la méthode ngOnInit().
  ngOnInit() {
    // Avec Subject, souscrit à ce Subject.
    this.appareilSubscription = this.appareilService.appareilListSubject.subscribe(
      (appareilList: any[]) =>
      this.appareilList = appareilList;
    );
    this.appareilService.emitAppareilListSubject();
  }

  onAllumer() {
    this.appareilService.switchOnAll();
  }

  onEteindre() {
    if (confirm("Etes-vous sûr de vouloir éteindre tous vos appareils ?"))
      this.appareilService.switchOffAll();
    else
      return null;
  }

  // Avec Subject, détruit la Subscription.
  ngOnDestroy() {
    this.appareilSubscription.unsubscribe();
  }

}
