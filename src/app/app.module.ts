import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PremierComponent } from './premier/premier.component';
import { AppareilComponent } from './appareil/appareil.component';
import { AuthComponent } from './auth/auth.component';
import { AppareilsListViewComponent } from './appareils-list-view/appareils-list-view.component';
import { AppareilSoloViewComponent } from './appareil-solo-view/appareil-solo-view.component';
import { AppareilSingleComponent } from './appareil-single/appareil-single.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';

import { AppareilService } from './services/appareil.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { EditAppareilComponent } from './edit-appareil/edit-appareil.component';

const appRoutes: Routes = [
  { path: 'auth', component: AuthComponent },
  { path: 'appareils-list', canActivate: [AuthGuardService], component: AppareilsListViewComponent },
  { path: 'appareils-list/:id', canActivate: [AuthGuardService], component: AppareilSingleComponent },
  { path: 'edit-appareil', canActivate: [AuthGuardService], component: EditAppareilComponent },
  { path: 'appareil-solo', canActivate: [AuthGuardService], component: AppareilSoloViewComponent },
  { path: 'premier-component', component: PremierComponent },
  { path: '', canActivate: [AuthGuardService], component: AppareilsListViewComponent }, // home
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    PremierComponent,
    AppareilComponent,
    AuthComponent,
    AppareilsListViewComponent,
    AppareilSoloViewComponent,
    AppareilSingleComponent,
    FourOhFourComponent,
    EditAppareilComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AppareilService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
