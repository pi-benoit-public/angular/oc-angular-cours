import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AppareilService } from '../services/appareil.service';

/*
 * Formulaire simple géré par la méthode template, l'utilisateur peut créer un
 * nouvel appareil pour la liste d'appareils.
 */
@Component({
  selector: 'app-edit-appareil',
  templateUrl: './edit-appareil.component.html'
})
export class EditAppareilComponent implements OnInit {

  defaultOnOff = "off";

  constructor(
    private appareilService: AppareilService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    // console.log(form.value);
    const name = form.value['name'];
    const status = form.value['status'];
    this.appareilService.addAppareil(name, status);
    this.router.navigate(['/appareils-list']);
  }

}
