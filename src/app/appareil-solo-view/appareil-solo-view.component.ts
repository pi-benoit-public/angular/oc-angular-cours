import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appareil-solo-view',
  templateUrl: './appareil-solo-view.component.html'
})
export class AppareilSoloViewComponent implements OnInit {

  // Appareil solo.
  appareilOne = 'lave-vaisselle';
  appareilTwo = 'micro-onde';
  appareilThree = 'four';

  constructor() { }

  ngOnInit() {
  }

}
