import { Subject } from 'rxjs/Subject';

export class AppareilService {

  onlyList: string = '1';

  // // Avec Subject, crée un Subject.
  appareilListSubject = new Subject<any[]>();

  // Avec Subject - mettre l'appareilList en private.
  private appareilList = [
    {
      id: 1,
      name: "ordinateur",
      status: "on"
    },
    {
      id: 2,
      name: "tablette",
      status: "off"
    },
    {
      id: 3,
      name: "smartphone",
      status: "on"
    }
  ];

  addAppareil(name: string, status: string) {
    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };
    appareilObject.id = this.appareilList[(this.appareilList.length - 1)].id + 1;
    appareilObject.name = name;
    appareilObject.status = status;
    this.appareilList.push(appareilObject);
    this.emitAppareilListSubject();
  }

  /*
   * Avec Subject, créer une méthode qui, quand le service reçoit de nouvelles
   * données, fait émettre ces données par le Subject et appele cette méthode
   * dans toutes les méthodes qui en ont besoin.
   * L'utilisation d'un Subject pour gérer la mise à jour des appareils
   * électriques permettra de mettre en place un niveau d'abstraction afin
   * d'éviter des bugs potentiels avec la manipulation de données.
   * Avec slice fait une copie de appareilList.
   */
  emitAppareilListSubject() {
    this.appareilListSubject.next(this.appareilList.slice());
  }

  getAppareilById(id: number) {
      const appareil = this.appareilList.find(
        (s) => {
          return s.id === id;
        }
      );
      return appareil;
  }

  switchOnAll() {
    for (let appareil of this.appareilList) {
      appareil.status = 'on';
    }
    // Avec Subject, Appel de la méthode.
    this.emitAppareilListSubject();
  }

  switchOffAll() {
    for (let appareil of this.appareilList) {
      appareil.status = 'off';
    }
    this.emitAppareilListSubject();
  }

  switchOnOne(i: number) {
    this.appareilList[i].status = 'on';
    this.emitAppareilListSubject();
  }

  switchOffOne(i: number) {
    this.appareilList[i].status = 'off';
    this.emitAppareilListSubject();
  }

}
