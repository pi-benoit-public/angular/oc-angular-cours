import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil-single',
  templateUrl: './appareil-single.component.html'
})
export class AppareilSingleComponent implements OnInit {

  name: string = "Appareil";
  status: string = "Statut";

  constructor(
    private appareilService: AppareilService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    // +id => cast la variable id en nombre.
    this.name = this.appareilService.getAppareilById(+id).name;
    this.status = this.appareilService.getAppareilById(+id).status;
  }

}
