import { Component, Input, OnInit } from '@angular/core';

import { AppareilService } from '../services/appareil.service';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html'
})
export class AppareilComponent implements OnInit {

  // @Input() c'est le décorateur.
  // Chaque instance a un nom différent régler à l'extérieur du code.
  // Ce décorateur a créé une propriété appareilName que l'on fixe dans app-appareil.
  @Input() appareilName: string;
  @Input() appareilStatus: string;
  @Input() index: number;
  @Input() id: number;
  @Input() onlyList: string;

  constructor(private appareilService: AppareilService) { }

  ngOnInit() {
  }

  getAppareilStatus() {
    return this.appareilStatus;
  }

  getColor() {
    if (this.appareilStatus == "on")
      return 'green';
    else
      return 'red'
  }

  onSwitch() {
    if (this.appareilStatus == "on")
      this.appareilService.switchOffOne(this.index);
    else
      this.appareilService.switchOnOne(this.index);
  }

}
