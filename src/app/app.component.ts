import { Component, OnInit, onDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
// Implémentation de l'interface OnInit dans la déclaration de classe avec implements OnInit.
export class AppComponent implements OnInit, onDestroy {

  title = 'OC Angular Cours';
  secondes: number;
  counterSubscription: Subscription;

  constructor() {}

  ngOnInit() {
    /*
     * Observable qui envoie un nouveau chiffre toutes les secondes (avec 1000).
     */
    const counter = Observable.interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Oups, il y a une erreur venant de l\'Observable : '+ error);
      },
      // complete
      () => {
        console.log('Observable complete ! ');
      }
    );
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

}
